# Modified from StyleGAN2-ADA &mdash; Official PyTorch implementation

## github page
https://github.com/NVlabs/stylegan2-ada-pytorch

## prepare images
python dataset_tool.py --source=image_dir --dest=out_dir/mydataset.zip

## train
python train.py --outdir=~/training-runs --data=~/mydataset.zip --gpus=4

## inference
test.ipynb

